<?php
	require_once('db.php');
	
	$reqType = test_input($_POST['reqType']);
	
	if($reqType == "profile"){
		$empId = test_input($_POST['empId']);
		$query = "select * from user where emp_id = ".$empId;
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		echo json_encode($row);
	}
	elseif($reqType == "emp_list"){
		$query = 'select name from employees order by name';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "emp_list_details"){
		$query = 'select * from user order by emp_id';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "mail_admin"){
		$query = 'select * from admin_inbox order by id desc';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "vendors_list"){
		$query = 'select * from vendor order by name';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "months"){
		$query = 'select distinct month from consumable_reimbursement order by month';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "t_reimbursement"){
		$emp_name = test_input($_POST['name']);
		$query = 'select * from travel_reimbursement where emp_name = "'.$emp_name.'" and status = "applied"';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "c_reimbursement"){
		$emp_name = test_input($_POST['name']);
		$query = 'select * from consumable_reimbursement where emp_name = "'.$emp_name.'" and status = "applied"';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "ta_reimbursement"){
		$emp_name = test_input($_POST['name']);
		$month = test_input($_POST['month']);
		$query = 'select * from travel_reimbursement where emp_name = "'.$emp_name.'" and month = "'.$month.'" and status = "approved"';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "ca_reimbursement"){
		$emp_name = test_input($_POST['name']);
		$month = test_input($_POST['month']);
		$query = 'select * from consumable_reimbursement where emp_name = "'.$emp_name.'" and month = "'.$month.'" and status = "approved"';
		$result = $conn->query($query);
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		echo json_encode($rows);
	}
	elseif($reqType == "dailyTask"){
		$id = test_input($_POST['id']);
		$query = 'select * from task where id = "'.$id.'"';
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		echo json_encode($row);
	}
	elseif($reqType == "addTask"){
		$start = test_input($_POST['tstart']);
		$end = test_input($_POST['tend']);
		$employee = test_input($_POST['temployee']);
		$title = test_input($_POST['ttitle']);
		$description = test_input($_POST['tdesc']);
		
		$query = 'insert into task (id, start, end, emp_name, title, description) values (NULL, "'.$start.'", "'.$end.'", "'.$employee.'", "'.$title.'", "'.$description.'")';
		
		if($conn->query($query)){
			echo "Task added successfully.";
		}else{
			echo "Server error: unable to add record.";
		}
	}
	elseif($reqType == "updateTask"){
		$id = test_input($_POST['id']);
		
		$query = 'update task set status = "submitted" where id = "'.$id.'"';
		
		if($conn->query($query)){
			echo "Records successfully updated. Please refresh page.";
		}else{
			echo "Server error: Unable to update record.";
		}
	}
	elseif($reqType == "approveTask"){
		$id = test_input($_POST['id']);
		
		$query = 'update task set status = "approved" where id = "'.$id.'"';
		
		if($conn->query($query)){
			echo "Records successfully updated. Please refresh page.";
		}else{
			echo "Server error: Unable to update record.";
		}
	}
	elseif($reqType == "addBlog"){
		if($_FILES["file"]["name"] != ''){
			$btitle = test_input($_POST['btitle']);
			$bcontent = test_input($_POST['bcontent']);
			
			$name = $_FILES["file"]["name"];
			$location = '../../images/blogs/'.$name;  
			
			$targetPath = test_input('images/blogs/'.$name);
			
			$query = 'insert into blogs (id, title, content, imageurl, date) values (NULL, "'.$btitle.'", "'.$bcontent.'", "'.$targetPath.'", CURRENT_DATE())';
			if($conn->query($query)){
				move_uploaded_file($_FILES["file"]["tmp_name"], $location);
				echo "Blog added successfully";
			}else{
				echo "Server error: unable to add blog.";
			}
		}
	}
	elseif($reqType == "picChange"){
		if($_FILES["file"]["name"] != ''){
			$emp_id = test_input($_POST['emp_id']);

			$name = $_FILES["file"]["name"];
			$location = '../images/profile/'.$name;  
			
			$targetPath = test_input('images/profile/'.$name);
			
			$query = 'update user set imageurl = "'.$targetPath.'" where emp_id = "'.$emp_id.'"';
			if($conn->query($query)){
				move_uploaded_file($_FILES["file"]["tmp_name"], $location);
				echo "Picture added successfully";
			}else{
				echo "Server error: unable to add picture.";
			}
		}
	}
	elseif($reqType == "mail_user"){
		$emp_name = test_input($_POST['uname']);

		$query = 'select * from employee_inbox where emp_name = "'.$emp_name.'" order by id desc';
		
		$result = $conn->query($query);
		
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		
		if($rows != null){
			echo json_encode($rows);
		}else{
			echo "no mails found in the inbox.";
		}
	}
	elseif($reqType == "login"){
		$email = test_input($_POST['umail']);
		$pwd = test_input($_POST['upwd']);

		$query = 'select * from employees where email = "'.$email.'" and password = "'.$pwd.'"';
		$res = $conn->query($query);
		$row1 = $res->fetch_assoc();
		if($row1 != null){
			$query = 'select * from user where email = "'.$email.'"';
			$result = $conn->query($query);
			$row = $result->fetch_assoc();
			if($row != null){
				echo json_encode($row);
			}else{
				echo "Invalid credentials, try again.";
			}
		}else{
			echo "Invalid credentials, try again.";
		}
	}
	elseif($reqType == "updateProfile"){
		$emp_id = test_input($_POST['pempid']);
		$contact_no = test_input($_POST['pcontact']);
		$highest_edu = test_input($_POST['pedu']);
		$prev_employer = test_input($_POST['pprev']);
		$emergency_c_name = test_input($_POST['pemename']);
		$emergency_c_no = test_input($_POST['pemecontact']);
		$adhaar_no = test_input($_POST['puid']);
		$pan_no = test_input($_POST['ppan']);
		
		$query = 'update user set highest_edu = "'.$highest_edu.'", prev_employer = "'.$prev_employer.'", contact_no = "'.$contact_no.'", emergency_c_name = "'.$emergency_c_name.'", emergency_c_no = "'.$emergency_c_no.'", adhaar_no = "'.$adhaar_no.'", pan_no = "'.$pan_no.'" where emp_id = "'.$emp_id.'"';
		if($conn->query($query)){
			echo "Profile updated successfully.";
		}else{
			echo "Server error: unable to update profile.";
		}
	}
	elseif($reqType == "amessage"){
		$name = test_input($_POST['mname']);
		$email = test_input($_POST['memail']);
		$subject = test_input($_POST['msubject']);
		$message = test_input($_POST['mmessage']);
		
		$query = 'insert into admin_inbox (id, name, email, subject, message, date) values(NULL, "'.$name.'", "'.$email.'", "'.$subject.'", "'.$message.'", CURRENT_DATE())';
		
		if($conn->query($query)){
			echo "Message successfully sent.";
		}else{
			echo "Server error: sending failed.";
		}
	}
	elseif($reqType == "emessage"){
		$name = test_input($_POST['mempname']);
		$subject = test_input($_POST['msubject']);
		$message = test_input($_POST['mmessage']);

		$query = 'insert into employee_inbox (id, emp_name, subject, message, date) values(NULL, "'.$name.'", "'.$subject.'", "'.$message.'", CURRENT_DATE())';
		
		if($conn->query($query)){
			echo "Message successfully sent.";
		}else{
			echo "Server error: sending failed.";
		}
	}
	elseif($reqType == "reply"){
		$name = test_input($_POST['mname']);
		$subject = test_input($_POST['msubject']);
		$message = test_input($_POST['mmessage']);
		$re = "Re: ";
		
		$query = 'insert into employee_inbox (id, emp_name, subject, message, date) values(NULL, "'.$name.'", "'.$re.$subject.'", "'.$message.'", CURRENT_DATE())';
		
		if($conn->query($query)){
			echo "Message successfully sent.";
		}else{
			echo "Server error: sending failed.";
		}
	}
	elseif($reqType == "onemail_employee"){
		$mail_id = test_input($_POST['mail_id']);
		
		$query = 'select * from employee_inbox where id = "'.$mail_id.'"';
		
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		if($row != null){
			echo json_encode($row);
		}else{
			echo "Server error: unable to fetch mail.";
		}		
	}
	elseif($reqType == "onemail_admin"){
		$mail_id = test_input($_POST['mail_id']);
		
		$query = 'select * from admin_inbox where id = "'.$mail_id.'"';
		
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		if($row != null){
			echo json_encode($row);
		}else{
			echo "Server error: unable to fetch mail.";
		}		
	}
	elseif($reqType == "treimbursement"){
		$date = test_input($_POST['tdate']);
		$month = test_input($_POST['tmonth']);
		$type = test_input($_POST['ttype']);
		$source = test_input($_POST['tfrom']);
		$destination = test_input($_POST['tto']);
		$distance = test_input($_POST['tdistance']);
		$fare = test_input($_POST['tfare']);
		$details = test_input($_POST['tdetails']);
		$emp_name = test_input($_POST['name']);
		
		$query = 'insert into travel_reimbursement (id, emp_name, date, month, type, source, destination, distance, fare, details, status) values (NULL, "'.$emp_name.'", "'.$date.'", "'.$month.'","'.$type.'", "'.$source.'", "'.$destination.'", "'.$distance.'", "'.$fare.'", "'.$details.'", "applied")';
		
		if($conn->query($query)){
			echo "Travel reimbursement requested successfully.";
		}else{
			echo "Server error: cannot submit query.";
		}
	}
	elseif($reqType == "creimbursement"){
		$date = test_input($_POST['cdate']);
		$month = test_input($_POST['cmonth']);
		$invoice = test_input($_POST['cinvoice']);
		$price = test_input($_POST['cprice']);
		$details = test_input($_POST['cdetails']);
		$emp_name = test_input($_POST['name']);
		
		$query = 'insert into consumable_reimbursement (id, emp_name, date, month, invoice_no, price, details, status) values (NULL, "'.$emp_name.'", "'.$date.'", "'.$month.'", "'.$invoice.'", "'.$price.'", "'.$details.'", "applied")';
		
		if($conn->query($query)){
			echo "Consumable reimbursement requested successfully.";
		}else{
			echo "Server error: cannot submit query.";
		}
	}
	elseif($reqType == "vendor"){
		$name = test_input($_POST['vname']);
		$address = test_input($_POST['vaddress']);
		$contact = test_input($_POST['vmno']);
		$email = test_input($_POST['vmail']);
		$details = test_input($_POST['vdetails']);
		
		$query = 'insert into vendor (id, name, address, contact_no, email, details) values (NULL, "'.$name.'", "'.$address.'", "'.$contact.'", "'.$email.'", "'.$details.'")';
		
		if($conn->query($query)){
			echo "Vendor details added successfully.";
		}else{
			echo "Server error: cannot submit query";
		}
		
	}
	elseif($reqType == "addEmployee"){
		$emp_id = test_input($_POST['aid']);
		$name = test_input($_POST['aname']);
		$mail = test_input($_POST['amail']);
		$start = test_input($_POST['astart']);
		$position = test_input($_POST['aposition']);
		
		$query = 'insert into employees (id, emp_id, name, email, position, start_date) values (NULL, "'.$emp_id.'", "'.$name.'", "'.$mail.'", "'.$position.'", "'.$start.'")';
		
		if($conn->query($query)){
			echo "Employee added.";
			$query = 'insert into user (id, emp_id, name, email, highest_edu, prev_employer, contact_no, emergency_c_name, emergency_c_no, adhaar_no, pan_no, start_date, status, position) values (NULL, "'.$emp_id.'", "'.$name.'", "'.$mail.'", "", "", "", "", "", "", "", "'.$start.'", "active", "'.$position.'")';
			if($conn->query($query)){
				echo "Records successfully added.";
			}else{
				echo "Server error: unable to add record.";
			}
		}else{
			echo "Server error: unable to add record.";
		}
	}
	elseif($reqType == "vdetails"){
		$id = test_input($_POST['vendor_id']);
		
		$query = 'select * from vendor where id = "'.$id.'"';
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		if($row != null){
			echo json_encode($row);
		}else{
			echo "Server error: Unable to fetch data.";
		}
	}
	elseif($reqType == "trdetails"){
		$id = test_input($_POST['t_id']);
		
		$query = 'select * from travel_reimbursement where id = "'.$id.'"';
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		if($row != null){
			echo json_encode($row);
		}else{
			echo "Server error: Unable to fetch data.";
		}
	}
	elseif($reqType == "crdetails"){
		$id = test_input($_POST['c_id']);
		
		$query = 'select * from consumable_reimbursement where id = "'.$id.'"';
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		if($row != null){
			echo json_encode($row);
		}else{
			echo "Server error: Unable to fetch data.";
		}
	}
	elseif($reqType == "tapprove"){
		$id = test_input($_POST['t_id']);
		
		$query = 'update travel_reimbursement set status = "approved" where id = "'.$id.'"';

		if($conn->query($query)){
			echo "Records updated successfully.";
		}else{
			echo "Server error: Unable to update records.";
		}
	}
	elseif($reqType == "treject"){
		$id = test_input($_POST['t_id']);
		
		$query = 'update travel_reimbursement set status = "rejected" where id = "'.$id.'"';

		if($conn->query($query)){
			echo "Records updated successfully.";
		}else{
			echo "Server error: Unable to update records.";
		}
	}
	elseif($reqType == "capprove"){
		$id = test_input($_POST['c_id']);
		
		$query = 'update consumable_reimbursement set status = "approved" where id = "'.$id.'"';

		if($conn->query($query)){
			echo "Records updated successfully.";
		}else{
			echo "Server error: Unable to update records.";
		}
	}
	elseif($reqType == "creject"){
		$id = test_input($_POST['c_id']);
		
		$query = 'update consumable_reimbursement set status = "rejected" where id = "'.$id.'"';

		if($conn->query($query)){
			echo "Records updated successfully.";
		}else{
			echo "Server error: Unable to update records.";
		}
	}
	elseif($reqType == "changePass"){
		$pnew = test_input($_POST['pnew']);
		$emp_id = test_input($_POST['id']);
		
		$query = 'update employees set password = "'.$pnew.'" where emp_id = "'.$emp_id.'"';
		
		if($conn->query($query)){
			echo "Records updated successfully.";
		}else{
			echo "Server error: Unable to update records.";
		}
	}
	elseif($reqType == "ttot"){
		$name = test_input($_POST['name']);
		
		$query = 'select sum(fare) as fare from travel_reimbursement where emp_name = "'.$name.'" and status = "applied"';
		
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		echo json_encode($row);
	}
	elseif($reqType == "tatotal"){
		$name = test_input($_POST['name']);
		$month = test_input($_POST['month']);
		
		$query = 'select sum(fare) as fare from travel_reimbursement where emp_name = "'.$name.'" and month = "'.$month.'" and status = "approved"';
		
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		echo json_encode($row);
	}
	elseif($reqType == "ctot"){
		$name = test_input($_POST['name']);
		
		$query = 'select sum(price) as price from consumable_reimbursement where emp_name = "'.$name.'" and status = "applied"';
		
		$result = $conn->query($query);
		$row2 = $result->fetch_assoc();
		echo json_encode($row2);
	}
	elseif($reqType == "catotal"){
		$name = test_input($_POST['name']);
		$month = test_input($_POST['month']);
		
		$query = 'select sum(price) as price from consumable_reimbursement where emp_name = "'.$name.'" and month = "'.$month.'" and status = "approved"';
		
		$result = $conn->query($query);
		$row2 = $result->fetch_assoc();
		echo json_encode($row2);
	}elseif($reqType == "checkPass"){
		$id = test_input($_POST['id']);
		
		$query = 'select password from employees where emp_id = "'.$id.'"';
		
		$result = $conn->query($query);
		$row2 = $result->fetch_assoc();
		echo json_encode($row2);
	}
	elseif($reqType == "delete_vendor"){
		$vid = test_input($_POST['vid']);
		
		$query = 'delete from vendor where id = "'.$vid.'"';
		
		if($conn->query($query)){
			echo "Records removed successfully";
		}else{
			echo "Server error: unable to remove record.";
		}
	}
	else{
		echo "Invalid request type.";
	}
	
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>