$(document).ready(function(){
	angular.element("#admin-controller").scope().checkLogin();
	angular.element("#admin-controller").scope().$apply();
	$(document).on('submit', '#blog-form', function(){
		var name = document.getElementById("file").files[0].name;
		var form_data = new FormData();
		var ext = name.split('.').pop().toLowerCase();
		if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
		{
			alert("Invalid Image File");
		}
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("file").files[0]);
		var f = document.getElementById("file").files[0];
		var fsize = f.size||f.fileSize;
		if(fsize > 2000000)
		{
			alert("Image File Size is very big");
		}
		else
		{
			form_data.append("file", document.getElementById('file').files[0]);
			var btitle = document.getElementById('btitle').value;
			var bcontent = document.getElementById('bcontent').value;
			form_data.append("btitle", btitle);
			form_data.append("bcontent", bcontent);
			form_data.append("reqType", "addBlog");
			$.ajax({
				url:"server/query.php",
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				processData: false,
				beforeSend:function(){
					
				},   
				success:function(data)
				{
					if(data == "Server error: unable to add blog."){
						alert(data + '\n please remove special chars from filename such as "(", ")", "-", "_", "@", etc.');
					}else{
						alert(data);
						breset();
					}
				}
			});
		}
	});
	
	$(document).on('submit', '#pic-change', function(){
		var name = document.getElementById("file2").files[0].name;
		var empId = $.cookie("emp_id");
		alert(empId);
		var form_data = new FormData();
		var ext = name.split('.').pop().toLowerCase();
		if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
		{
			alert("Invalid Image File");
		}
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("file2").files[0]);
		var f = document.getElementById("file2").files[0];
		var fsize = f.size||f.fileSize;
		if(fsize > 2000000)
		{
			alert("Image File Size is very big");
		}
		else
		{
			form_data.append("file", document.getElementById('file2').files[0]);
			form_data.append("reqType", "picChange");
			form_data.append("emp_id", empId);
			$.ajax({
				url:"server/query.php",
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				processData: false,
				beforeSend:function(){
					
				},   
				success:function(data){
					alert(data);
				}
			});
		}
	});
	
});

var app = new angular.module("admin", ['ngCookies', 'angularUtils.directives.dirPagination']);

app.controller("admin-controller", function($scope, $http, $cookies, $window){
	var isLoggedIn = $cookies.get('isLoggedIn');
	var name = $cookies.get('username');
	$scope.checkLogin = function(){
		if(isLoggedIn == "yes"){
			if(name == 'Admin'){
				$scope.initializeAdmin();
			}else{
				alert("You are not authorized to use this page, please login.");
				$window.location.assign('index.html');
			}
		}else{
			$window.location.assign('index.html');
		}
	}
	$scope.logOut = function(){
		$cookies.put('isLoggedIn', '');
		$cookies.put('username', '');
		$cookies.put('emp_id', '');
		$cookies.put('email', '');
		$window.location.assign('index.html');
	}
	
	$scope.initializeAdmin = function(){
		$scope.profile();
		$scope.employeeList();
		$scope.fetchEmployees();
		$scope.inboxAdmin();
		$scope.fetchVendors();
		$scope.fetchMonths();
	}
	
	$scope.profile = function(){
		var id = $cookies.get('emp_id');
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'profile', empId: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.user = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	$scope.employeeList = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'emp_list'}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.names = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.fetchEmployees = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'emp_list_details'}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.employees = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.inboxAdmin = function(ngTableParams){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'mail_admin'}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.mails = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.fetchVendors = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'vendors_list'}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.vendors = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.fetchMonths = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'months'}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.months = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.reimbursements = function(){
		$scope.treimbursements();
		$scope.creimbursements();
		$scope.ctotal();
		$scope.ttotal();
		
	}
	$scope.treimbursements = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 't_reimbursement', name: $scope.emp_name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.treimbursement = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.creimbursements = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'c_reimbursement', name: $scope.emp_name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.creimbursement = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.ttotal = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'ttot', name: $scope.emp_name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.ttot = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.ctotal = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'ctot', name: $scope.emp_name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.ctot = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.areimbursements = function(){
		$scope.tareimbursements();
		$scope.careimbursements();
		$scope.tatotal();
		$scope.catotal();
	}
	$scope.tareimbursements = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'ta_reimbursement', name: $scope.emp_name_approved, month: $scope.month_approved}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.tareimbursement = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.careimbursements = function(){
		var postReq2 = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'ca_reimbursement', name: $scope.emp_name_approved, month: $scope.month_approved}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq2).then(success, error);
		
		function success(response){
			$scope.careimbursement = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.tatotal = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'tatotal', name: $scope.emp_name_approved, month: $scope.month_approved}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.tapproved = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.catotal = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'catotal', name: $scope.emp_name_approved, month: $scope.month_approved}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$scope.capproved = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.update = function(){
		var emp_id = $cookies.get('emp_id');
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'updateProfile', pempid: emp_id, pcontact: $scope.pcontact, pedu: $scope.pedu, pprev: $scope.pprev, pemename: $scope.pemename, pemecontact: $scope.pemecontact, puid: $scope.puid, ppan: $scope.ppan}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.fetchMail = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'onemail_admin', mail_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.mail = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.vdetails = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'vdetails', vendor_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.vendor = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.trdetails = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'trdetails', t_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.travel = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.crdetails = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'crdetails', c_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.consumable = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.tapprove = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'tapprove', t_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.treject = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'treject', t_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.capprove = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'capprove', c_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.creject = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'creject', c_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.empProfile = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'profile', empId: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.emp = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.changePass = function(){
		var emp_id = $cookies.get('emp_id');
		var pass = $scope.pnew;
		var cpass = $scope.cpass;
		
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'checkPass', id: emp_id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			var res = angular.fromJson(response.data);
			if(res.password == $scope.pold){
				if(pass == cpass){
					var postReq = {
						method: 'POST',
						url: 'server/query.php', 
						data: $.param({reqType: 'changePass', pnew: $scope.pnew, id: emp_id}),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					};
					$http(postReq).then(success, error);
					
					function success(response){
						$window.alert(response.data);
						preset();
					}
					function error(response){
						$window.alert("Error.");
						$window.alert(response.data);
						preset();
					}
				}else{
					$scope.error = "Password did not match, type again";
					preset();
				}
			}else{
				$scope.error = "Old password did not match, type again";
				preset();
			}
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.fillCalendar = function(){
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listWeek'
			},
			defaultView: 'month',
			navLinks: true, // can click day/week names to navigate views
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: {
				url: 'server/atask.php?q=' + $scope.empName
			},
			eventClick: function(event){
				$('#task-one').modal('show');
				$scope.oneTask(event.id);
			},
			eventAfterRender: function(event, element){
				var status = event.status;
				if(status == "incomplete"){
					element.css('background-color', '#F44336');
				}else if(status == "approved"){
					element.css('background-color', '#43A047');
				}else{
					element.css('background-color', '#FFEB3B');
				}
			}
		});
	}
	
	$scope.oneTask = function(taskId){
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'dailyTask', id: taskId}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}; 
		
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.taskOne = response.data;
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.approve = function(taskId){
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'approveTask', id: taskId}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}; 
		
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.reply = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'reply', mname: $scope.mail.name, msubject: $scope.mail.subject, mmessage: $scope.rmessage}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}; 
		
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
			rreset();
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.vdelete = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'delete_vendor', vid: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}; 
		
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert(response.data);
		}
	}
});

function rreset(){
	document.getElementById("reply-form").reset();
}
			
function breset(){
	document.getElementById("blog-form").reset();
}

function preset(){
	document.getElementById("password-form").reset();
}

var xhttp = new XMLHttpRequest;

function message(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("message-form").reset();
		}
	};
	
	var mempname = document.getElementById('mempname').value;
	var msubject = document.getElementById('msubject').value;
	var mmessage = document.getElementById('mmessage').value;
	
	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=emessage&mempname="+mempname+"&msubject="+msubject+"&mmessage="+mmessage);
}
function vendor(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("vendor-form").reset();
		}
	};
	
	var vname = document.getElementById('vname').value;
	var vaddress = document.getElementById('vaddress').value;
	var vmno = document.getElementById('vmno').value;
	var vmail = document.getElementById('vmail').value;
	var vdetails = document.getElementById('vdetails').value;

	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=vendor&vname="+vname+"&vaddress="+vaddress+"&vmno="+vmno+"&vmail="+vmail+"&vdetails="+vdetails);
}
function task(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("task-form").reset();
		}
	};
	
	var tstart = document.getElementById('tstart').value;
	var tend = document.getElementById('tend').value;
	var temployee = document.getElementById('temployee').value;
	var ttitle = document.getElementById('ttitle').value;
	var tdesc = document.getElementById('tdesc').value;
	
	var start = Date.parse(tstart);
	var end = Date.parse(tend);
	if(end < start){
		alert("Task end date should be larger than start date.");
		return;
	}
	
	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=addTask&tstart="+tstart+"&tend="+tend+"&temployee="+temployee+"&ttitle="+ttitle+"&tdesc="+tdesc);
}
function employee(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("addemployee-form").reset();
		}
	};
	
	var aid = document.getElementById('aid').value;
	var aname = document.getElementById('aname').value;
	var amail = document.getElementById('amail').value;
	var astart = document.getElementById('astart').value;
	var aposition = document.getElementById('aposition').value;
	
	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=addEmployee&aid="+aid+"&aname="+aname+"&amail="+amail+"&astart="+astart+"&aposition="+aposition);
}