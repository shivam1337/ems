var app = angular.module("login", ['ngCookies']);
			
app.controller('login-controller', function($scope, $http, $cookies, $window, $location){
	$scope.login = function(){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'login', umail: $scope.umail, upwd: $scope.upwd}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			if(response.data == "Invalid credentials, try again."){
				alert(response.data);
			}else{
				$scope.res = angular.fromJson(response.data);
				$scope.userName = $scope.res.name;
				$window.alert($scope.res.name);
				$cookies.put('isLoggedIn', 'yes');
				$cookies.put('username', $scope.res.name);
				$cookies.put('emp_id', $scope.res.emp_id);
				$cookies.put('email', $scope.res.email);
				if($scope.res.position == "admin"){
					$window.location.assign('admin.html#profile-admin');
				}else{
					$window.location.assign('employee.html#profile-employee');
				}
			}
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
});