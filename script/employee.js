$(document).ready(function(){
	angular.element("#employee-controller").scope().checkLogin();
	angular.element("#employee-controller").scope().$apply();
	$(document).on('submit', '#pic-change', function(){
		var name = document.getElementById("file2").files[0].name;
		var empId = $.cookie("emp_id");
		alert(empId);
		var form_data = new FormData();
		var ext = name.split('.').pop().toLowerCase();
		if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
		{
			alert("Invalid Image File");
		}
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("file2").files[0]);
		var f = document.getElementById("file2").files[0];
		var fsize = f.size||f.fileSize;
		if(fsize > 2000000)
		{
			alert("Image File Size is very big");
		}
		else
		{
			form_data.append("file", document.getElementById('file2').files[0]);
			form_data.append("reqType", "picChange");
			form_data.append("emp_id", empId);
			$.ajax({
				url:"server/query.php",
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				processData: false,
				beforeSend:function(){
					
				},   
				success:function(data){
					alert(data);
					creset();
				}
			});
		}
	});
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listWeek'
		},
		defaultView: 'month',
		navLinks: true, // can click day/week names to navigate views
		editable: false,
		eventLimit: true, // allow "more" link when too many events
		events: {
			url: 'server/task.php'
		},
		eventClick: function(event){
			$('#task-one').modal('show');
			angular.element('#employee-controller').scope().oneTask(event.id);
		},
		eventAfterRender: function(event, element){
			var status = event.status;
			if(status == "incomplete"){
				element.css('background-color', '#F44336');
			}else if(status == "approved"){
				element.css('background-color', '#43A047');
			}else{
				element.css('background-color', '#FFEB3B');
			}
		}
	});
});

function creset(){
	document.getElementById("file2").reset();
}
var app = new angular.module("employee", ['ngCookies', 'angularUtils.directives.dirPagination']);

app.controller("employee-controller", function($scope, $http, $cookies, $window){
	$scope.checkLogin = function(){
		var id = $cookies.get('emp_id');
		var isLoggedIn = $cookies.get('isLoggedIn');
		var name = $cookies.get('username');
		
		if(isLoggedIn == "yes"){
			if(name != "Admin"){
				$window.alert("Welcome back, " + name);
				$scope.profile();
				$scope.inbox();
			}else{
				$window.alert("You are not authorized to use this page, please login.");
				$window.location.assign('index.html');
			}
			
		}else{
			$window.alert("You are not authorized to use this page, please login.");
			$window.location.assign('index.html');
		}
	}
	
	$scope.profile = function(){
		var id = $cookies.get('emp_id');
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'profile', empId: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.user = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.inbox = function(){
		var name = $cookies.get('username');
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'mail_user', uname: name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.mails = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.fetchMail = function(id){
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'onemail_employee', mail_id: id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.mail = response.data;
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.logOut = function(){
		$cookies.put('isLoggedIn', '');
		$cookies.put('username', '');
		$cookies.put('emp_id', '');
		$window.location.assign('index.html');
	}
	
	$scope.update = function(){
		var emp_id = $cookies.get('emp_id');
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'updateProfile', pempid: emp_id, pcontact: $scope.pcontact, pedu: $scope.pedu, pprev: $scope.pprev, pemename: $scope.pemename, pemecontact: $scope.pemecontact, puid: $scope.puid, ppan: $scope.ppan}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.travel = function(){
		var emp_name = $cookies.get('username');
		var date = angular.element('#tdate').val();
		var month = date.slice(0, 7);
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'treimbursement', tdate: date, tmonth: month, ttype: $scope.ttype, tfrom: $scope.tfrom, tto: $scope.tto, tdistance: $scope.tdistance, tfare: $scope.tfare, tdetails: $scope.tdetails, name: emp_name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}
		$http(postReq).then(success, error);
		function success(response){
			$window.alert(response.data);
			treset();
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.consumable = function(){
		var emp_name = $cookies.get('username');
		var date = angular.element('#cdate').val();
		var month = date.slice(0, 7);
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'creimbursement', cdate: date, cmonth: month, cinvoice: $scope.cinvoice, cprice: $scope.cprice, cdetails: $scope.cdetails, name: emp_name}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}
		$http(postReq).then(success, error);
		function success(response){
			$window.alert(response.data);
			creset();
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.changePass = function(){
		var emp_id = $cookies.get('emp_id');
		var pass = $scope.pnew;
		var cpass = $scope.cpass;
		
		var postReq = {
			method: 'POST',
			url: 'server/query.php', 
			data: $.param({reqType: 'checkPass', id: emp_id}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
		$http(postReq).then(success, error);
		
		function success(response){
			var res = angular.fromJson(response.data);
			if(res.password == $scope.pold){
				if(pass == cpass){
					var postReq = {
						method: 'POST',
						url: 'server/query.php', 
						data: $.param({reqType: 'changePass', pnew: $scope.pnew, id: emp_id}),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					};
					$http(postReq).then(success, error);
					
					function success(response){
						$window.alert(response.data);
						preset();
					}
					function error(response){
						$window.alert("Error.");
						$window.alert(response.data);
						preset();
					}
				}else{
					$scope.error = "Password did not match, type again";
					preset();
				}
			}else{
				$scope.error = "Old password did not match, type again";
				preset();
			}
		}
		function error(response){
			$window.alert("Error.");
			$window.alert(response.data);
		}
	}
	
	$scope.oneTask = function(taskId){
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'dailyTask', id: taskId}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}; 
		
		$http(postReq).then(success, error);
		
		function success(response){
			$scope.taskOne = response.data;
		}
		function error(response){
			$window.alert(response.data);
		}
	}
	
	$scope.complete = function(taskId){
		var postReq = {
			method: 'POST',
			url: 'server/query.php',
			data: $.param({reqType: 'updateTask', id: taskId}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}; 
		
		$http(postReq).then(success, error);
		
		function success(response){
			$window.alert(response.data);
		}
		function error(response){
			$window.alert(response.data);
		}
	}
});

function preset(){
	document.getElementById("password-form").reset();
}
function creset(){
	document.getElementById("consumable-form").reset();
}

function treset(){
	document.getElementById("travel-form").reset();
}
var xhttp = new XMLHttpRequest;
function message(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("message-form").reset();
		}
	};
	
	var mname = $.cookie('username');
	var memail = $.cookie('email');
	var msubject = document.getElementById('msubject').value;
	var mmessage = document.getElementById('mmessage').value;
	
	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=amessage&mname="+mname+"&memail="+memail+"&msubject="+msubject+"&mmessage="+mmessage);
}

function vendor(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("vendor-form").reset();
		}
	};
	
	var vname = document.getElementById('vname').value;
	var vaddress = document.getElementById('vaddress').value;
	var vmno = document.getElementById('vmno').value;
	var vmail = document.getElementById('vmail').value;
	var vdetails = document.getElementById('vdetails').value;
	
	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=vendor&vname="+vname+"&vaddress="+vaddress+"&vmno="+vmno+"&vmail="+vmail+"&vdetails="+vdetails);
}

function task(){
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(this.responseText);
			document.getElementById("task-form").reset();
		}
	};
	
	var tstart = document.getElementById('tstart').value;
	var tend = document.getElementById('tend').value;
	var temployee = $.cookie('username');
	var ttitle = document.getElementById('ttitle').value;
	var tdesc = document.getElementById('tdesc').value;
	
	xhttp.open("POST", "server/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("reqType=addTask&tstart="+tstart+"&tend="+tend+"&temployee="+temployee+"&ttitle="+ttitle+"&tdesc="+tdesc);
}