-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 26, 2019 at 11:56 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_inbox`
--

DROP TABLE IF EXISTS `admin_inbox`;
CREATE TABLE IF NOT EXISTS `admin_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `message` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `admin_inbox`
--

INSERT INTO `admin_inbox` (`id`, `name`, `email`, `subject`, `message`, `date`) VALUES
(16, 'Uchiha Sky', 'uchiha.sky@gmail.com', '8', '8', '2019-06-26'),
(15, 'Uchiha Sky', 'uchiha.sky@gmail.com', '7', '7', '2019-06-26'),
(11, 'Uchiha Sky', 'uchiha.sky@gmail.com', 'Hello', 'Hello 2', '2019-06-26'),
(12, 'Uchiha Sky', 'uchiha.sky@gmail.com', 'See', 'See 3', '2019-06-26'),
(13, 'Uchiha Sky', 'uchiha.sky@gmail.com', '4', '4', '2019-06-26'),
(14, 'Uchiha Sky', 'uchiha.sky@gmail.com', '5', '5', '2019-06-26'),
(10, 'Uchiha Sky', 'uchiha.sky@gmail.com', 'Hello', 'Hello 1', '2019-06-26'),
(7, 'ghost', 'ghost@gmail.com', 'Test for cookies', 'Cookie mail service test 1.', '2019-06-26'),
(8, 'ghost', 'ghost@gmail.com', 'Daily report', 'Reporting from the site, daily report.', '2019-06-26'),
(9, 'Uchiha Sky', 'uchiha.sky@gmail.com', 'First time report', 'Reporting for the first time since joining.', '2019-06-26'),
(17, 'Uchiha Sky', 'uchiha.sky@gmail.com', '9', '9', '2019-06-26'),
(18, 'Uchiha Sky', 'uchiha.sky@gmail.com', '10', '10', '2019-06-26');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8_general_mysql500_ci NOT NULL,
  `content` varchar(500) COLLATE utf8_general_mysql500_ci NOT NULL,
  `imageurl` varchar(30) COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `content`, `imageurl`, `date`) VALUES
(1, 'Sample blog title 1', 'Sample blog content 1.', 'images/blogs/img1.jpg', '2019-05-29'),
(2, 'Sample blog title 2', 'Sample blog content 2.', 'images/blogs/img2.jpg', '2019-05-29'),
(3, 'Sample blog title 3', 'Sample blog content 3', 'images/blogs/img3.jpg', '2019-05-29'),
(21, 'Blog After merging files.', 'Something good is about to happen.\r\nKeep your eyes open for it.', 'images/blogs/526887.jpg', '2019-06-21');

-- --------------------------------------------------------

--
-- Table structure for table `consumable_reimbursement`
--

DROP TABLE IF EXISTS `consumable_reimbursement`;
CREATE TABLE IF NOT EXISTS `consumable_reimbursement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` date NOT NULL,
  `month` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `invoice_no` varchar(30) COLLATE utf8_general_mysql500_ci NOT NULL,
  `price` int(11) NOT NULL,
  `details` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'applied',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `consumable_reimbursement`
--

INSERT INTO `consumable_reimbursement` (`id`, `emp_name`, `date`, `month`, `invoice_no`, `price`, `details`, `status`) VALUES
(4, 'ghost', '2019-06-14', '2019-06', '123456789', 500, 'I bought something for 500rs.', 'rejected'),
(6, 'ghost', '2019-06-14', '2019-06', '123456487', 500, '500rs product.', 'approved'),
(7, 'ghost', '2019-06-18', '2019-06', '963852741', 3500, 'Bought something.', 'approved'),
(8, 'ghost', '2019-06-19', '2019-06', '789456123', 852, 'asd85', 'applied'),
(9, 'ghost', '2019-07-11', '2019-07', '741852963', 852, 'Something I bought.', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_general_mysql500_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `type` varchar(8) COLLATE utf8_general_mysql500_ci NOT NULL,
  `message` varchar(500) COLLATE utf8_general_mysql500_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `status`, `name`, `email`, `subject`, `type`, `message`) VALUES
(1, 'uncheked', 'Shivam Marmat', 'marmat.shivam99@gmail.com', 'Form test 9', 'support', 'test 9.'),
(3, 'uncheked', 'Shivam Marmat', 'marmat.shivam99@gmail.com', 'Form test 11', 'inquiry', 'Form test 10.\r\ntype: inquiry.\r\nrunning in localhost.'),
(4, 'uncheked', 'GhostOfMidnight', 'ghostofmidnight1337@gmail.com', 'Form test 12', 'support', 'Form test 12.\r\ntype: Support.\r\nrunning in localhost.\r\nghost is sending a support request.'),
(5, 'uncheked', 'Shivam Marmat', 'ghostofmidnight1337@gmail.com', 'Test 13', 'support', 'Form test 13.\r\nTesting in mobile view.'),
(7, 'uncheked', 'Shivam Marmat', 'marmat.shivam99@gmail.com', 'Form test 14', 'support', 'Form test 14.\nUsing Ajax.'),
(8, 'uncheked', 'Ghost', 'ghost@gmail.com', 'ghost', 'support', 'ghost is sending support query.'),
(9, 'uncheked', 'shivam', 'shivam@gmail.com', 'hello', 'support', 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `position` varchar(20) COLLATE utf8_general_mysql500_ci NOT NULL,
  `start_date` date NOT NULL,
  `password` varchar(20) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'Test@123',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `emp_id`, `name`, `email`, `position`, `start_date`, `password`) VALUES
(3, 20, 'ghost', 'ghost@gmail.com', 'employee', '2019-06-14', 'Test@1234'),
(4, 25, 'manager 1', 'manager.1@gmail.com', 'manager', '2019-06-14', 'Test@123'),
(5, 1, 'Admin', 'admin@gmail.com', 'admin', '2019-06-17', 'Test@123'),
(9, 25, 'Uchiha Sky', 'uchiha.sky@gmail.com', 'Web Developer', '2019-06-25', 'Test@123');

-- --------------------------------------------------------

--
-- Table structure for table `employee_inbox`
--

DROP TABLE IF EXISTS `employee_inbox`;
CREATE TABLE IF NOT EXISTS `employee_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `message` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `employee_inbox`
--

INSERT INTO `employee_inbox` (`id`, `emp_name`, `subject`, `message`, `date`) VALUES
(1, 'ghost', 'Hi', 'Moshi Moshi admin desu.', '2019-06-18'),
(2, 'ghost', 'Hello', 'Hi ghost,\n     How are things in the NetherRealm. Hope you are enjoying your stay over there.\nExpecting great results from you.', '2019-06-18'),
(3, 'ghost', 'Something.', 'Something.', '2019-06-21'),
(4, 'Uchiha Sky', 'Re: First time report', 'Good work on first report.', '2019-06-26');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `price` int(11) NOT NULL,
  `imageurl` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `imageurl`) VALUES
(1, 'kettle', 'Kettle, with its revolutionary technology can change the world.', 1234, 'images/products/kettle.png'),
(2, 'Super cool air conditioner', 'Super cool air conditioner can easily cool your room like you are in Himalaya.', 12345, 'images/products/ac.png'),
(3, 'High speed blender 4000', 'High speed blender will change the way you think of blending food.', 1423, 'images/products/blender.png'),
(4, 'Steam cooker 10000', 'Steam cooker 10000 cooks rice or steams vegetables in the blink of the eye.', 1324, 'images/products/cooker.png'),
(5, 'Food micer x20', 'Food mixer x20 can mix dough in minutes without any efforts.', 1432, 'images/products/foodmixer.png'),
(6, 'Wind Blade Mixer', 'With power of wind blade this mixer can mix and cut many solid stuff.', 1342, 'images/products/mixer.png'),
(7, 'Snow blaze Refrigerator', 'Snow blaze refrigerator can freeze water quickly and keep food and vegetable fresh for long time.', 21342, 'images/products/refrigirator.png');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `emp_name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'incomplete',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `start`, `end`, `emp_name`, `title`, `description`, `status`) VALUES
(1, '2019-06-18', '2019-06-19', 'ghost', 'Sample task.', 'Ghost must do this task.', 'approved'),
(2, '2019-06-19', '2019-06-21', 'ghost', 'Sample task', 'Sample task', 'approved'),
(4, '2019-06-25', '2019-06-28', 'ghost', 'Test Calendar 1', 'Testing Calendar for loading event.\nTest 1.', 'incomplete'),
(5, '2019-06-26', '2019-06-27', 'ghost', 'Add task test 1', 'Testing add task feature in the employee section.\nTest 1.', 'approved'),
(6, '2019-07-07', '2019-07-07', 'ghost', 'Some Task', 'This task is essential and needs to be done.', 'incomplete'),
(7, '2019-06-25', '2019-06-26', 'ghost', 'xyz', '1.\n2.\n3.', 'submitted'),
(8, '2019-06-25', '2019-06-18', 'ghost', '123', '456', 'incomplete'),
(11, '2019-06-26', '2019-06-29', 'Uchiha Sky', 'Long task', 'Long task', 'incomplete'),
(10, '2019-06-26', '2019-06-27', 'Uchiha Sky', 'task', 'task', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `techspecs`
--

DROP TABLE IF EXISTS `techspecs`;
CREATE TABLE IF NOT EXISTS `techspecs` (
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

-- --------------------------------------------------------

--
-- Table structure for table `travel_reimbursement`
--

DROP TABLE IF EXISTS `travel_reimbursement`;
CREATE TABLE IF NOT EXISTS `travel_reimbursement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` date NOT NULL,
  `month` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_general_mysql500_ci NOT NULL,
  `source` varchar(20) COLLATE utf8_general_mysql500_ci NOT NULL,
  `destination` varchar(20) COLLATE utf8_general_mysql500_ci NOT NULL,
  `distance` int(11) NOT NULL,
  `fare` int(11) NOT NULL,
  `details` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'applied',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `travel_reimbursement`
--

INSERT INTO `travel_reimbursement` (`id`, `emp_name`, `date`, `month`, `type`, `source`, `destination`, `distance`, `fare`, `details`, `status`) VALUES
(1, 'ghost', '2019-06-14', '2019-06', 'cab', 'Shivajinagar', 'Kothrud', 20, 50, 'Traveled for business purpose.', 'approved'),
(2, 'ghost', '2019-06-14', '2019-06', 'train', 'Shivajinagar', 'Akurdi', 50, 100, 'Travel for business purpose.', 'rejected'),
(3, 'ghost', '2019-06-18', '2019-06', 'cab', 'Shivajinagar', 'Hinjawadi', 40, 100, 'Business visit', 'applied'),
(4, 'ghost', '2019-06-19', '2019-06', 'bus', 'Shivajinagar', 'Hinjawadi', 50, 100, 'Business purpose.', 'applied'),
(5, 'ghost', '2019-06-19', '2019-06', 'bus', 'Shivajinagar', 'Hinjawadi', 50, 100, 'Business purpose.', 'approved'),
(6, 'ghost', '2019-06-19', '2019-06', 'cab', 'Shivajinagar', 'Kothrud', 30, 500, 'Business purpose.', 'applied'),
(7, 'ghost', '2019-06-18', '2019-06', 'train', 'Shivajinagar', 'Viman nagar', 50, 100, 'Business purpose.', 'applied'),
(9, 'ghost', '2019-07-02', '2019-07', 'train', 'Shivajinagar', 'Akurdi', 50, 50, 'Traveled by train.', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `highest_edu` varchar(30) COLLATE utf8_general_mysql500_ci NOT NULL,
  `prev_employer` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `contact_no` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `emergency_c_name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `emergency_c_no` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `adhaar_no` varchar(12) COLLATE utf8_general_mysql500_ci NOT NULL,
  `pan_no` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `start_date` date NOT NULL,
  `status` varchar(8) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'active',
  `position` varchar(20) COLLATE utf8_general_mysql500_ci NOT NULL,
  `imageurl` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'images/person.png',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `emp_id`, `name`, `email`, `highest_edu`, `prev_employer`, `contact_no`, `emergency_c_name`, `emergency_c_no`, `adhaar_no`, `pan_no`, `start_date`, `status`, `position`, `imageurl`) VALUES
(2, 20, 'ghost', 'ghost@gmail.com', 'B.tech', 'Shiranai', '7410852963', 'Don', '8527410963', '123456789789', '852CPNM963', '2019-06-14', 'active', 'employee', 'images/profile/120600.jpg'),
(3, 25, 'manager 1', 'manager.1@gmail.com', 'B.tech IT', 'fresher', '7418529635', 'Uhiha Itachi', '9638527418', '741852963852', '741CFPM963', '2019-06-14', 'active', 'manager', 'images/profile/120600.jpg'),
(4, 1, 'Admin', 'admin@gmail.com', 'M.tech', 'company', '9638520741', 'Admin ka bhai', '7410852963', '963852741852', '852CFPM963', '2019-06-17', 'active', 'admin', 'images/profile/maxresdefault.jpg'),
(6, 25, 'Uchiha Sky', 'uchiha.sky@gmail.com', 'B.tech IT', 'fresher', '7418529635', 'Uhiha Itachi', '9638527418', '741852963852', '741CFPM963', '2019-06-25', 'active', 'Web Developer', 'images/profile/120600.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
CREATE TABLE IF NOT EXISTS `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `contact_no` varchar(10) COLLATE utf8_general_mysql500_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_general_mysql500_ci NOT NULL,
  `details` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `name`, `address`, `contact_no`, `email`, `details`) VALUES
(11, 'Vendor 2', 'address of vendor 2.', '9876543210', 'vendor.2@gmail.com', 'vendor 2.'),
(12, 'Vendor 3', 'vendor 3 lives here.', '9898989898', 'vendor.3@gmail.com', 'I\'m vendor 3.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
